package com.thinkin_service.app.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.thinkin_service.app.R;

public class TermsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
    }
}
